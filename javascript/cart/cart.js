let cart = []
const cartContainer = document.querySelector("#carritoContenedor")
const totalPrice = document.querySelector("#precioTotal")
const goToCartPage = document.querySelector("#goToCartPage")

document.addEventListener('DOMContentLoaded', () => {
	cart = JSON.parse(localStorage.getItem("cart")) || []
	showCart()
})

goToCartPage.addEventListener('click', () => {
	if (cart.length === 0) {
		Swal.fire({
			title: "Your cart is Empty", 
			text: "Buy something to go to Cart Page",
			icon: "error", 
			confirmButtonText: "OK"
		})
	} else {
		location.href = "/html/cartWithProduct.html"
	}
})

function addProductFromStock(prodID, stock, quantity) {
	const exists = cart.some(prod => prod.id === prodID) 

	if (!exists) {
		const item = stock.find((prod) => prod.id === prodID)
		cart.push(item)
	}

	let prod = getItemFromCart(prodID)
	prod.inCart += quantity; 

	if (prod.inCart > prod.stock) {
		prod.inCart = prod.stock
	}

	showCart()
}

function emptyCart() {
	cart.forEach((prod) => {
		prod.inCart = 0
	})
	cart = []
	showCart()
}

const showCart = () => { 
	const modalBody = document.querySelector('.modal .modal-body')

	cart = cart.filter(prod => prod.inCart > 0)
	modalBody.innerHTML = ""
	cart.forEach((prod) => {
		const {image, id, title, rating, reviews, price, stock, description, alternatives, inCart} = prod;
			modalBody.innerHTML += `
			<div class="modal-contenedor"> 
			<div> 
				<img class="img-carrito" src="${image}"/>
			</div>
	
			<div class="cart-data"> 
				<p>${title}</p>
				<p>Price: $ ${price}</p>
				<p>Quantity: ${inCart}</p>
			</div>
	
			<button class = "btn btn-danger" onclick="removeProduct(${id})">Remove Product</button>
			</div>
			`
	})

	cartContainer.textContent = cartTotalItems();
	totalPrice.textContent = "$ " + cartTotalPrice();
	saveStorage()
}

function removeProduct(id) {
	let item = getItemFromCart(id)
	item.inCart = 0; 
	showCart()
}

function saveStorage() {
	localStorage.setItem("cart", JSON.stringify(cart))
}

function getItemFromCart(prodID) {
	let item = cart.find((prod) => prod.id === prodID)
	return item; 
}

function cartTotalItems() {
	let totalItems = 0; 
	cart.forEach((prod) => {
		totalItems += prod.inCart
	})
	return totalItems;
}

function cartTotalPrice() {
	let totalPrice = 0; 
	cart.forEach((prod) => {
		totalPrice += (prod.inCart * prod.price)
	})
	return totalPrice;
}

function updateInCartValue(productId, quantity) {
	let item = getItemFromCart(productId);
	item.inCart = quantity
	showCart()
}