delay(10).then(() => {
    loadProducts()
    ready()
})
    
function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

function loadProducts() {
    let productPageCart = document.querySelector("#product-page-cart")
    productPageCart.innerHTML = ""
    cart.forEach((product) => {
		const {image, id, title, rating, reviews, price, stock, description, alternatives, inCart} = product;
        productPageCart.innerHTML += `
        <div class="product">
        <div class="init">
          <p>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</p>
        </div>
        <div class="img">
          <img src="${image}" alt="Gamming Mouse">
        </div>
        <div class="title">
        <h1>${title}</h1>
        </div>
        
        <div class="stars">
        <h6>Id:<span class="id-value"> ${id}</span></h6>
         
          <div class="rating">
          <h2 class="reviews">${rating}</h2>
          </div>
          <div> 
          <h2 class="price">
          $ ${price}
          </h2>
            
          </div> 
          <h2 class="stock">${stock}</h2>
          <span id="minus" class="qt-minus" onclick="">-</span>
          <span class="qt">${inCart}</span>
          <span id="max" class="qt-plus">+</span>
        </div>
        <div class="deleteItem">
          <button class="material-symbols-outlined" onclick="removeProduct(${id})">
            delete</button>
        </div>
      </div>
        `
    })
}

function ready() {
  updateCartAndItemsTotal();

  var itemContainer = document.getElementsByClassName("cart")[0];
    var product = itemContainer.getElementsByClassName("product");
  for (var i = 0; i < product.length; i++) {
    var cartRow = product[i]
    updateStars(cartRow);
  }
    var removeItemButton = document.getElementsByClassName("deleteItem")
        for (var index = 0; index < removeItemButton.length; index++) {
                var button = removeItemButton[index]
                button.addEventListener('click', removeCartItem)
        }

    var increaseItemButton = document.getElementsByClassName("qt-plus")
        for (var index = 0; index < increaseItemButton.length; index++) {
            var buttonIn = increaseItemButton[index]
            buttonIn.addEventListener('click', increaseMontItem)
        }
    var decreaseItemButton = document.getElementsByClassName("qt-minus")
        for (var index = 0; index < decreaseItemButton.length; index++) {
            var buttonIn = decreaseItemButton[index]
            buttonIn.addEventListener('click', decreaseMontItem)
        }
}

function removeCartItem(event){
    var buttonClicked = event.target
    buttonClicked.parentElement.parentElement.remove();
    updateCartAndItemsTotal();
}

function increaseMontItem(event){
    var buttonClicked = event.target
    var quantity = buttonClicked.parentNode.getElementsByClassName("qt")[0];
    var stock = buttonClicked.parentNode.getElementsByClassName("stock")[0];
    let idLabel = buttonClicked.parentNode.getElementsByClassName("id-value")[0];
    var quantityTotal =  parseFloat(quantity.innerText);
    var stockTotal =  parseFloat(stock.innerText);
    let id = parseFloat(idLabel.innerText);
    
    if(quantityTotal < stockTotal){
      quantity.innerText = quantityTotal + 1;
      updateInCartValue(id, quantityTotal + 1);
      updateCartAndItemsTotal();
    }
}

function decreaseMontItem(event){
    var buttonClicked = event.target
    var quantity = buttonClicked.parentNode.getElementsByClassName("qt")[0];
    let idLabel = buttonClicked.parentNode.getElementsByClassName("id-value")[0];
    var quantityTotal =  parseFloat(quantity.innerText.replace('$', ''));
    let id = parseFloat(idLabel.innerText);

    if(quantityTotal > 1){
      quantity.innerText = quantityTotal - 1;
      updateInCartValue(id, quantityTotal - 1);
        updateCartAndItemsTotal();
    }
}

function updateStars(event){
  var product = event;
  var rating = product.getElementsByClassName("reviews")[0];
  var ratingPro = parseFloat(rating.innerText);
  for (let index = 0; index < ratingPro; index++) {
    let greeting = event.querySelector('.rating');
    greeting.innerHTML += `
    <span class="fa fa-star checked"></span>
    `;
  }
  if(ratingPro < 5){
    for (let index = ratingPro; index < 5; index++) {
      let greeting = event.querySelector('.rating');
      greeting.innerHTML += `
      <span class="fa fa-star star"></span>
      `;
    }
  }
}

function updateCartAndItemsTotal(){
    var itemContainer = document.getElementsByClassName("cart")[0]
    var product = itemContainer.getElementsByClassName("product");
    var total = 0
    var items = 0;
    if(product.length == 0){
        location.href = "/html/cart.html"
      }else{
        for (var i = 0; i < product.length; i++) {
          var cartRow = product[i]
          var priceElement = cartRow.getElementsByClassName("price")[0]
          var quantityElement = cartRow.getElementsByClassName("qt")[0]
          var price = parseFloat(priceElement.innerText.replace('$', ''))
          var quantity =  parseFloat(quantityElement.innerText);
          total = total + (price * quantity);
          items = items + quantity;
        }
        total = Math.round(total * 100) / 100;
        document.getElementsByClassName("order")[0].innerText = '$' + total;
        document.getElementsByClassName("mont")[0].innerText = '$' + total;
        document.getElementsByClassName("values")[0].innerText = items + ' items';
        document.getElementsByClassName("quantity")[0].innerText = '(' + product.length + ')';
    }


}