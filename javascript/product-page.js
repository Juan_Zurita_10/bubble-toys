var MainImg = document.getElementById("MainImg");
var smallImg = document.getElementsByClassName("small-img");

smallImg[0].onclick = function() {
    MainImg.src = smallImg[0].src;
}

smallImg[1].onclick = function() {
    MainImg.src = smallImg[1].src;
}

smallImg[2].onclick = function() {
    MainImg.src = smallImg[2].src;
}

smallImg[3].onclick = function() {
    MainImg.src = smallImg[3].src;
}

const jsons = {"action-toys": stockProductsActionToys, "baby-games": stockbabytoys, "board-games": stockBoardtoys, 
"construction-toys": stockConstructiontoys, "customes": stockCustomes, "dolls": stockDolls, "stuffed-toys": stockStuffedtoys, "home": stockHome};

let jsonPath = document.referrer.slice(38, -5);
let currJson = jsons[jsonPath];

let jsonId = window.name;
if (typeof currJson == 'undefined') {
    currJson = jsons["home"];
}
console.log(jsonPath);
console.log(jsonId);
console.log(currJson);

document.getElementById("MainImg").src = currJson[jsonId]["image"];
document.getElementById("title").innerHTML = currJson[jsonId]["title"];
document.getElementById("reviews").innerHTML = currJson[jsonId]["reviews"];
document.getElementById("price").innerHTML = "$" + currJson[jsonId]["price"];
document.getElementById("stock").innerHTML = currJson[jsonId]["stock"];
document.getElementById("description").innerHTML = currJson[jsonId]["description"];
document.getElementById("alternative1").src = currJson[jsonId]["alternatives"][0];
document.getElementById("alternative2").src = currJson[jsonId]["alternatives"][1];
document.getElementById("alternative3").src = currJson[jsonId]["alternatives"][2];
document.getElementById("alternative4").src = currJson[jsonId]["alternatives"][3];

let addToCartButton = document.getElementById("addToCart");

addToCartButton.addEventListener("click", function() {
    addProductFromStock(currJson[jsonId]["id"], currJson, parseFloat(quantity.value));
})

let ratingSegments = Array.from(document.getElementsByClassName("star"));
let rating = currJson[jsonId]["rating"];

for (let index = 0; index < rating; index++) {
    console.log(ratingSegments[index].src);
    ratingSegments[index].src = "../images/star-active.png";
}

var decreaseButton = document.getElementById("decrease-quantity");
decreaseButton.disabled = true;
var increaseButton = document.getElementById("increase-quantity");
var quantity = document.getElementById("pro-quantity");
var stock = currJson[jsonId]["stock"];
if (quantity.value == stock) {
    increaseButton.disabled = true;
}

increaseButton.addEventListener('click', increase);
function increase() {
    console.log("Is " + document.getElementById("pro-quantity").value + " < " + currJson[jsonId]["stock"]);
    if (document.getElementById("pro-quantity").value < currJson[jsonId]["stock"]) {
        var quantity = document.getElementById("pro-quantity").value;
        quantity++;
        document.getElementById("pro-quantity").value = quantity++;
    }
    if (document.getElementById("pro-quantity").value == currJson[jsonId]["stock"]) {
        increaseButton.disabled = true;
    }
    if (document.getElementById("pro-quantity").value == 2) {
        decreaseButton.disabled = false;
    }
}

decreaseButton.addEventListener('click', decrease);
function decrease() {
    if (document.getElementById("pro-quantity").value > 1) {
        var quantity = document.getElementById("pro-quantity").value;
        quantity--;
        document.getElementById("pro-quantity").value = quantity--;
    }
    if (document.getElementById("pro-quantity").value < currJson[jsonId]["stock"]) {
        increaseButton.disabled = false;
    }
    if (document.getElementById("pro-quantity").value == 1) {
        decreaseButton.disabled = true;
    }
}
