let stockHome = [
    {
        "id": 54,
        "image": "../../images/stuffed-toys/cowmuuv3.png",
        "title": "Stuffed Muu cow",
        "rating": 4,
        "reviews": 2,
        "price": 18,
        "stock": 20,
        "inCart": 0,
        "description": "Here is bread man, an ingenious croissant who wants to be by your side",
        "alternatives": []
    },

    {
        "id": 8,
        "image": "../../images/action-toys/iron-man.png",
        "title": "Bend And Flex Iron Man",
        "rating": 4,
        "reviews": 16,
        "price": 20.99,
        "stock": 17,
        "inCart": 0,
        "description": "Tony Stark designs his own super suit to become the tech-powered hero Iron Man. Twist! Turn! Flex your power! Kids can bend flex pose and play with their favorite Marvel Super Heroes with these super agile Avengers Bend and Flex Figures! Collect characters inspired by the Marvel Universe with a twist (each sold separately). These stylized Super Hero action figures have bendable arms and legs that can bend and hold in place for the picture-perfect pose! There?s plenty of heroic daring and dramatic action when kids shape their Bend And Flex figures into plenty of playful poses. The included accessory helps kids enhance the pose and play out favorite Marvel scenes. Create berserk battles with the hero adapting and flexing some fancy moves! After all a good hero (or villain) is always flexible. Copyright 2019 MARVEL. Hasbro and all related terms are trademarks of Hasbro.",
        "alternatives": ["../../images/action-toys/iron-man.png", "../../images/action-toys/iron-man.png"]
      },
    
      {
        "id": 9,
        "image": "../../images/action-toys/iron-gauntelet.png",
        "title": "Red Infinity Gauntlet",
        "rating": 4,
        "reviews": 16,
        "price": 22.99,
        "stock": 1,
        "inCart": 0,
        "description": "Imagine the incredible super-powered action of the Avengers with figures roleplay and more inspired by Avengers: Endgame! This Infinity Gauntlet is inspired by the Avengers: Endgame movie part of the Marvel Cinematic Universe that includes Avengers: Infinity War. Avengers: Infinity War features characters like Iron Man Captain America Spider-Man Black Panther and more! With Avengers: Endgame -inspired toys kids can imagine battling like their favorite heroes and playing their part in saving the galaxy! ",
        "alternatives": ["../../images/action-toys/iron-gauntelet.png", "../images/action-toys/iron-gauntelet-2.png"]
      }, 

      {
        "id": 33,
        "image": "../../images/construction-toys/Tiranidos1.png",
        "title": "The Tyranids",
        "rating": 4,
        "reviews": 1,
        "price": 100.90,
        "stock": 28,
        "inCart": 0,
        "description": "Toy of The Tyranids, its are an extragalactic composite species of hideous, insectoid xenos. They actually comprise an entire space-faring ecosystem comprised of innumerable different bioforms which are all variations on the same genetic theme",
        "alternatives": ["../../images/construction-toys/Tiranidos1.png", "../images/construction-toys/Tiranidos2.png", "../images/construction-toys/Tiranidos3.png"]
      }, {
        "id": 55,
        "image": "../../images/stuffed-toys/breadMan.jpeg",
        "title": "Stuffed Bread man",
        "rating": 5,
        "reviews": 10,
        "price": 8.99,
        "stock": 40,
        "inCart": 0,
        "description": "the Muu cow will be the one who accompanies you with its enviable softness and its beautiful bovine spots",
        "alternatives": []
    },

    {
        "id": 56,
        "image": "../../images/stuffed-toys/Doge.jpeg",
        "title": "little Doge",
        "rating": 0,
        "reviews": 0,
        "price": 23.99,
        "stock": 1,
        "inCart": 0,
        "description": "\nThe stuffed doge is the perfect mix between the tender and the fluffy, perfect to have it in front of you all day",
        "alternatives": []
    }
]