let stockCustomes = [
                      {
                        "id": 19,
                        "image": "../../images/disguises/unoDisguises1.png",
                        "title": "UNO Draw Four Children's Tabard Costume",
                        "rating": 4,
                        "reviews": 1,
                        "price": 49.90,
                        "stock": 19,
                        "inCart": 0,
                        "description": " You know UNO! It's the multi-age, multi-generational card game loved by people all over the world. That is, until you have to draw cutro or skip. Why not join your next novelty party? Become a full-fledged card game with this unisex, easy to wear, and colorful UNO family costume.",
                        "alternatives": ["../../images/disguises/unoDisguises1.png", "../images/disguises/unoDisguises2.png", "../images/disguises/unoDisguises3.png"]
                      },

                      {
                        "id": 37,
                        "image": "../../images/disguises/DrStrange-disguises1.png",
                        "title": "Dr Strange Deluxe Multiverse Costume",
                        "rating": 4,
                        "reviews": 1,
                        "price": 39.90,
                        "stock": 25,
                        "inCart": 0,
                        "description": " Dress up as Dr Strange this Halloween in this Doctor Strange in the Multiverse of Madness costume for adults. Become the sorcerer supreme and primary protector of Earth.",
                        "alternatives": ["../../images/disguises/DrStrange-disguises1.png", "../images/disguises/DrStrange-disguises2.png", "../images/disguises/DrStrange-disguises3.png"]
                      },

                      {
                        "id": 38,
                        "image": "../../images/disguises/duck1.png",
                        "title": "Duck Latex Overhead Mask",
                        "rating": 2,
                        "reviews": 1,
                        "price": 19.90,
                        "stock": 30,
                        "inCart": 0,
                        "description": "Quack Quack, the barnyard is calling! Full face Duck latex mask. This Halloween costume overhead full face masks are made from quality soft latex. Perfect as Donald for your next farmyard or Disney themed party. ",
                        "alternatives": ["../../images/disguises/duck1.png", "../images/disguises/duck2.png"]
                      },

                      {
                        "id": 39,
                        "image": "../../images/disguises/Squid-game1.png",
                        "title": "Squid Game Player Costume",
                        "rating": 2,
                        "reviews": 1,
                        "price": 59.90,
                        "stock": 25,
                        "inCart": 0,
                        "description": "from Netflix's cult series Calamari Game, come these costumes to celebrate in style.",
                        "alternatives": ["../../images/disguises/Squid-game1.png", "../images/disguises/Squid-game2.png", "../images/disguises/Squid-game3.png"]
                      },

                      {
                        "id": 40,
                        "image": "../../images/disguises/Squid-game-guard1.png",
                        "title": "Squid Game Guard Deluxe Costume",
                        "rating": 4,
                        "reviews": 1,
                        "price": 69.90,
                        "stock": 19,
                        "inCart": 0,
                        "description": " From the cult Netflix series The Squid Game, come these officially licensed deluxe guard costumes, the gardias from The Squid Game.",
                        "alternatives": [ "../../images/disguises/Squid-game-guard1.png", "../images/disguises/Squid-game-guard2.png"]
                      },

                      {
                        "id": 41,
                        "image": "../../images/disguises/PlagueDoctor1.png",
                        "title": "Plague Doctor Costume",
                        "rating": 4,
                        "reviews": 1,
                        "price": 79.90,
                        "stock": 100,
                        "inCart": 0,
                        "description": " During the Bubonic Plague, Plague Doctors beame a warning symbol of impeding doom, which makes this costume perfect for scary fun at Halloween. Costume includes long black robe with hooded cape and soft face mask with mesh covered eye holes.",
                        "alternatives": ["../../images/disguises/PlagueDoctor1.png", "../images/disguises/PlagueDoctor2.png", "../images/disguises/PlagueDoctor3.png"]
                      },

                      {
                        "id": 42,
                        "image": "../../images/disguises/dinosaurT-rex1.png",
                        "title": "Inflatable Dinosaur Costume T-Rex Jurassic Fancy Costume For Kids Adult",
                        "rating": 1,
                        "reviews": 1,
                        "price": 39.90,
                        "stock": 200,
                        "inCart": 0,
                        "description": "Inflatable Dinosaur Costume T-Rex Jurassic Fancy Costum. Dinosaur Costume Suit, great simple costume for fun dress parties, festivals & carnivals.",
                        "alternatives": ["../../images/disguises/dinosaurT-rex1.png", "../images/disguises/dinosaurT-rex2.png", "../images/disguises/dinosaurT-rex3.png"]
                      },

                      {
                        "id": 43,
                        "image": "../../images/disguises/picachu1.png",
                        "title": "Pokemon Pikachu Classic Costume",
                        "rating": 4,
                        "reviews": 1,
                        "price": 38.90,
                        "stock": 50,
                        "inCart": 0,
                        "description": "From Nintendo's Pokemon video game, comes this fully licensed Classic Pikachu costume. Costume is soft and cuddly, bright yellow onesie jumpsuit with tail and attached Pikachu hood.",
                        "alternatives": ["../../images/disguises/picachu1.png", "../images/disguises/picachu2.png"]
                      },

                      {
                        "id": 44,
                        "image": "../../images/disguises/IronMan1.png",
                        "title": "Iron Man Mask",
                        "rating": 4,
                        "reviews": 1,
                        "price": 100,
                        "stock": 59,
                        "inCart": 0,
                        "description": " Iron Man Mask Helmet/Manual Opening/Closing/Closing/Remote Control Portable Model Includes Platform, Helmet-OneSize",
                        "alternatives": ["../../images/disguises/IronMan1.png", "../images/disguises/IronMan2.png", "../images/disguises/IronMan3.png"]
                      },
                    ]