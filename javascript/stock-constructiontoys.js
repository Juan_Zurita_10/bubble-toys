let stockConstructiontoys = [
  {
    "id": 28,
    "image": "../../images/construction-toys/Horizon1.png",
    "title": "LEGO 76989 Horizon Forbidden West: Tallneck Set  ",
    "rating": 3,
    "reviews": 1,
    "price": 70.90,
    "stock": 50,
    "inCart": 0,
    "description": "Build the ultimate LEGO brick model (76989) of the most iconic Horizon machine in the Forbidden West: the Tallneck with this LEGO set ",
    "alternatives": ["../../images/construction-toys/Horizon1.png", "../images/construction-toys/Horizon2.png", "../images/construction-toys/Horizon3.png"]
  },

  {
    "id": 29,
    "image": "../../images/construction-toys/ATT1.png",
    "title": "LEGO Star Wars AT-AT Walker Model UCS Big Set ",
    "rating": 3,
    "reviews": 1,
    "price": 88.90,
    "stock": 50,
    "inCart": 0,
    "description": "Every LEGO Star Wars fan has waited for this AT-AT big LEGO Star Wars UCS set. This epic Ultimate Collector Series Star Wars buildable model features posable legs and head",
    "alternatives": ["../../images/construction-toys/ATT1.png", "../images/construction-toys/ATT2.png", "../images/construction-toys/ATT3.png"]
  },

  {
    "id": 30,
    "image": "../../images/construction-toys/TIE-Fighter1.png",
    "title": "LEGO Star Wars Imperial TIE Fighter Toy",
    "rating": 3,
    "reviews": 2,
    "price": 73.90,
    "stock": 60,
    "inCart": 0,
    "description": "Imagine adventures as villains from the classic space odyssey with a LEGO Star Wars brick build of the Imperial TIE Fighter. Capture the sleek design of an iconic starfighter in the Imperial fleet",
    "alternatives": ["../../images/construction-toys/TIE-Fighter1.png", "../images/construction-toys/TIE-Fighter2.png", "../images/construction-toys/TIE-Fighter3.png"]
  },

  {
    "id": 31,
    "image": "../../images/construction-toys/Hogwarts1.png",
    "title": "LEGO Harry Potter Hogwarts Castle Toy for Teens & Adults",
    "rating": 2,
    "reviews": 3,
    "price": 88.90,
    "stock": 58,
    "inCart": 0,
    "description": "This highly detailed LEGO Harry Potter Hogwarts Castle toy is sure to enchant fans of all ages. With this rare Harry Potter LEGO set, you can create a Great Hall with moving staircases, Hagrid’s hut and the Whomping Willow.",
    "alternatives": ["../../images/construction-toys/Hogwarts1.png", "../images/construction-toys/Hogwarts2.png", "../images/construction-toys/Hogwarts3.png"]
  },

  {
    "id": 32,
    "image": "../../images/construction-toys/SuperMarioCastle1.png",
    "title": "LEGO Super Mario Cat Peach Suit & Tower Expansion Set",
    "rating": 4,
    "reviews": 1,
    "price": 68.90,
    "stock": 45,
    "inCart": 0,
    "description": "Children can build a supercool LEGO Super Mario level with the Cat Peach Suit and Frozen Tower Expansion Set. Thrilling LEGO adventures with Princess Peach await in this castle toy.",
    "alternatives": ["../../images/construction-toys/SuperMarioCastle1.png", "../images/construction-toys/SuperMarioCastle3.png"]
  },

  {
    "id": 33,
    "image": "../../images/construction-toys/Tiranidos1.png",
    "title": "The Tyranids",
    "rating": 4,
    "reviews": 1,
    "price": 100.90,
    "stock": 28,
    "inCart": 0,
    "description": "Toy of The Tyranids, its are an extragalactic composite species of hideous, insectoid xenos. They actually comprise an entire space-faring ecosystem comprised of innumerable different bioforms which are all variations on the same genetic theme",
    "alternatives": ["../../images/construction-toys/Tiranidos1.png", "../images/construction-toys/Tiranidos2.png", "../images/construction-toys/Tiranidos3.png"]
  },

  {
    "id": 34,
    "image": "../../images/construction-toys/JurassicWorld1.png",
    "title": "LEGO Jurassic World Giganotosaurus Attack Dinosaur Toy",
    "rating": 4,
    "reviews": 1,
    "price": 38.90,
    "stock": 50,
    "inCart": 0,
    "description": "Jurassic World: Dominion fans can relive epic movie action with the awesome LEGO Jurassic World Giganotosaurus Attack toy playset.",
    "alternatives": ["../../images/construction-toys/JurassicWorld1.png", "../images/construction-toys/JurassicWorld2.png"]
  },

  {
    "id": 35,
    "image": "../../images/construction-toys/Ninjago1.png",
    "title": "LEGO NINJAGO Ninja Ultra Combo Mech & Toy Car 4",
    "rating": 4,
    "reviews": 1,
    "price": 38.90,
    "stock": 50,
    "inCart": 0,
    "description": "LEGO NINJAGO Ninja Ultra Combo Mech & Toy Car 4 in 1 Set features an action mech toy figure that is the ultimate fighting machine and consists of a mech, car, tank and jet that can all be played with separately.",
    "alternatives": ["../../images/construction-toys/Ninjago1.png", "../images/construction-toys/Ninjago2.png"]
  },

  {
    "id": 36,
    "image": "../../images/construction-toys/WoodenBlockSet1.png",
    "title": "Chad Valley PlaySmart Wooden Block Set - 80 Pieces",
    "rating": 4,
    "reviews": 1,
    "price": 38.90,
    "stock": 50,
    "description": "Help your children to unleash their creative side with this 80 piece Wooden Block Set. The different shaped blocks can be placed together to build whatever your child can think of",
    "alternatives": ["../images/construction-toys/WoodenBlockSet1.png", "../images/construction-toys/WoodenBlockSet2.png", "../images/construction-toys/WoodenBlockSet3.png"]
  }
]