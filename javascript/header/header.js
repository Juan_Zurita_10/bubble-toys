class Header extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `

    <script src="https://kit.fontawesome.com/67c66657c7.js"></script>
    
    <nav>
    <div class ="icon">             
        <a class="logo" href="/html/home.html" target="_parent">
            <img src="/images/bubble_toys_logo.png">
        </a>

        <div class="shop-div">
            <a href="/html/sections.html" target="_parent"> <span><img src="/images/shop_logo.png" alt="" class="shop-img"></span></a>
        </div>
        
    </div>


    <div class = "search-box">
        <input type="search" placeholder="Search here" disabled>
        <span class="fa fa-search"></span>
    </div>
    
    <ol>
        <li><a href="/html/sections.html" target="_parent"><span class="shop-span">Shop</span> </a></li>
        <li><a href="/html/login.html" target="_parent" class="disabled"><span><img src="/images/account_logo.png" alt="">Account</span></a></li>
        
        <li class="nav-item" id="cart-nav-item"><a class="" target="_parent" data-bs-toggle="modal" data-bs-target="#modal">
            <span><img src="/images/cart_logo.png" alt="">Cart <i id="carritoContenedor">0</i></span>
            
        </a></li>

    </ol>
</nav>
    `     
    }
  }
  
  customElements.define('header-component', Header);