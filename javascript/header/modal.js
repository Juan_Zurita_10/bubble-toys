class Modal extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `
      <div class="container-xl">
      <div class="modal" tabindex="-1" role="dialog" id="modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Cart</h5>
              <button
                type="button"
                class="close"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" id="vaciarCarrito" onclick="emptyCart()">
                Empty Cart
              </button>
              <button type="button" id="goToCartPage" class="btn btn-primary">
                Go to Cart Page
              </button>
              <button
                type="button"
                class="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
    
              <span class="total-price">Total price: <span id="precioTotal"></span></span>
              
            </div>
          </div>
        </div>
      </div>
    </div>

    `
      
    }
  }
  
  customElements.define('cart-modal', Modal);