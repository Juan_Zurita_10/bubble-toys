let stockProductsActionToys = [
  {
    "id": 1,
    "image": "/images/action-toys/bumblebee.png",
    "title": "Transformers Bumblebee Cyberverse",
    "rating": 4,
    "reviews": 23,
    "price": 46.99,
    "stock": 29,
    "inCart": 0,
    "description": "The battle between heroic Autobots and evil Decepticons continues on the Bumblebee Cyberverse Adventures animated series! As a powerful new threat targets Cybertron it is up to Grimlock and his new friends the Dinobots to unite and save the planet! Discover the unique powers and skills of each Cyberverse character and see how those powers will be used to defend Cybertron? or threaten it.",
    "alternatives": ["/images/action-toys/bumblebee.png", "../images/action-toys/bumblebee-2.png", "../images/action-toys/bumblebee-3.png"]
  },

  {
    "id": 2,
    "image": "/images/action-toys/mjolnir.png",
    "title": "Mjolnir Electronic Hammer",
    "rating": 3,
    "reviews": 12,
    "price": 33.99,
    "stock": 1,
    "inCart": 0,
    "description": "Once shattered by the powerful Hela Thor's magically enchanted hammer is back now controlled by a new hero worthy of its power! Imagine charging into battle with Mjolnir to protect New Asgard! Kids ages 5 and up can pretend to gear up as either Thor or Mighty Thor with the Mighty FX Mjolnir Electronic Hammer featuring 2 hero modes of play.",
    "alternatives": ["/images/action-toys/mjolnir.png", "../images/action-toys/mjolnir-2.png", "../images/action-toys/mjolnir-3.png"]
  },

  {
    "id": 3,
    "image": "/images/action-toys/black-panter.png",
    "title": "Black Panther Hero",
    "rating": 2,
    "reviews": 13,
    "price": 11.99,
    "stock": 30,
    "inCart": 0,
    "description": "12 scale Marvel Titan Hero Series action figure inspired by Black Panther featuring movie-inspired design and detail as well as multiple points of articulation for pose able fun boys and girls will love pretending to claw their way into battle with this large - scale Black Panther toy.",
    "alternatives": ["/images/action-toys/black-panter.png", "../images/action-toys/black-panter-2.png", "../images/action-toys/black-panter-3.png"]
  },

  {
    "id": 4,
    "image": "/images/action-toys/mario-bros.png",
    "title": "Nintendo Mario Figure",
    "rating": 5,
    "reviews": 10,
    "price": 49.99,
    "stock": 26,
    "inCart": 0,
    "description": "Bring Super Mario to life with the ultimate figure It's-A Me Mario Kids of all ages will have endless fun with the 12 tall Mario packed with over 30 phrases and sound effects from the games.",
    "alternatives": ["/images/action-toys/mario-bros.png", "../images/action-toys/mario-bros-2.png"]
  },

  {
    "id": 5,
    "image": "/images/action-toys/grimlock.png",
    "title": "Transformers Classic Grimlock",
    "rating": 5,
    "reviews": 11,
    "price": 11.99,
    "stock": 13,
    "inCart": 0,
    "description": "Imagine helping Grimlock save the day as a mighty T-rex. Grimlock is ready to take to the streets and charge onto the scene. Kids ages 3 and up can pretend there is a dangerous situation unfolding and convert the brave Grimlock action figure from a robot to a T-rex.",
    "alternatives": ["/images/action-toys/grimlock.png"]
  },

  {
    "id": 6,
    "image": "/images/action-toys/rider-spider.png",
    "title": "Flex Rider Spider-Man",
    "rating": 4,
    "reviews": 48,
    "price": 16.99,
    "stock": 99,
    "inCart": 0,
    "description": "Twist! Turn! Kids can imagine speeding to the scene on a motorcycle then changing the vehicle into high-tech weapons to thwart the villain! Kids can bend flex pose and play with their favorite Marvel Super Heroes with these super agile Bend and Flex Figures! Collect characters inspired by Marvel Universe with a twist (each sold separately).",
    "alternatives": ["/images/action-toys/rider-spider.png", "../images/action-toys/rider-spider-2.png"]
  },

  {
    "id": 7,
    "image": "/images/action-toys/batman-suit.png",
    "title": "Batman Action Figure",
    "rating": 1,
    "reviews": 1,
    "price": 14.99,
    "stock": 5,
    "inCart": 0,
    "description": "Level up for the ultimate battle with Tech Armor and Batman 4-inch Action Figure! Featuring exclusive 4-inch action figure of Batman and exclusive transforming Tech Armor. Ready to battle right out of the box! The Batman figure is highly detailed with Bat-Tech styling and 11 points of articulation to pose any way you want. Place Batman inside the blue Bat-Tech Armor close it up and prepare for battle! With Batman geared up in his powerful armor move the armor?s arms and face off against the Super Villains! Pose your Batman figure as you protect Gotham City and defend against the iconic Super-Villain! To remove Batman from the Tech Armor simply lift the top and pull your figure out. Add all of the 4-inch Batman Action Figures to your collection (each sold separately) for more exciting missions and battles! Bring the action of Batman home with Bat-Tech Batman!",
    "alternatives": ["/images/action-toys/batman-suit.png", "../images/action-toys/batman-suit-2.png", "../images/action-toys/batman-suit-3.png"]
  },

  {
    "id": 8,
    "image": "/images/action-toys/iron-man.png",
    "title": "Bend And Flex Iron Man",
    "rating": 4,
    "reviews": 16,
    "price": 20.99,
    "stock": 17,
    "inCart": 0,
    "description": "Tony Stark designs his own super suit to become the tech-powered hero Iron Man. Twist! Turn! Flex your power! Kids can bend flex pose and play with their favorite Marvel Super Heroes with these super agile Avengers Bend and Flex Figures! Collect characters inspired by the Marvel Universe with a twist (each sold separately). These stylized Super Hero action figures have bendable arms and legs that can bend and hold in place for the picture-perfect pose! There?s plenty of heroic daring and dramatic action when kids shape their Bend And Flex figures into plenty of playful poses. The included accessory helps kids enhance the pose and play out favorite Marvel scenes. Create berserk battles with the hero adapting and flexing some fancy moves! After all a good hero (or villain) is always flexible. Copyright 2019 MARVEL. Hasbro and all related terms are trademarks of Hasbro.",
    "alternatives": ["/images/action-toys/iron-man.png"]
  },

  {
    "id": 9,
    "image": "/images/action-toys/iron-gauntelet.png",
    "title": "Red Infinity Gauntlet",
    "rating": 4,
    "reviews": 16,
    "price": 22.99,
    "stock": 1,
    "inCart": 0,
    "description": "Imagine the incredible super-powered action of the Avengers with figures roleplay and more inspired by Avengers: Endgame! This Infinity Gauntlet is inspired by the Avengers: Endgame movie part of the Marvel Cinematic Universe that includes Avengers: Infinity War. Avengers: Infinity War features characters like Iron Man Captain America Spider-Man Black Panther and more! With Avengers: Endgame -inspired toys kids can imagine battling like their favorite heroes and playing their part in saving the galaxy! ",
    "alternatives": ["/images/action-toys/iron-gauntelet.png", "../images/action-toys/iron-gauntelet-2.png"]
  }
]