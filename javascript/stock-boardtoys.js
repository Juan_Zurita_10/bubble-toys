let stockBoardtoys =[
    {
        "id": 19,
        "image": "../../images/board-games/monopoly-img/monopoly.png",
        "title": "Monopoly: Classic Edition",
        "rating": 4,
        "reviews": 20,
        "price": 10.00,
        "stock": 10,
        "inCart": 0,
        "description": "Monopoly is a real-estate board game for two to eight players. The player's goal is to remain financially solvent while forcing opponents into bankruptcy by buying and developing pieces of property. Bankruptcy results in elimination from the game. The last player remaining on the board is the winner.",
        "alternatives": [
            "../../images/board-games/monopoly-img/monopoly.png",
            "../../images/board-games/monopoly-img/monopoly-2.png",
            "../../images/board-games/monopoly-img/monopoly-3.png",
            "../../images/board-games/monopoly-img/monopoly-4.png"
        ]
    },
    {
        "id": 20,
        "image": "../../images/board-games/guesswho-img/guess-who.jpg",
        "title": "Guess Who? Guessing Game",
        "rating": 5,
        "reviews": 45,
        "price": 12.75,
        "stock": 5,
        "inCart": 0,
        "description": "Each player chooses a mystery character and then using yes or no questions, they try to figure out the other player s mystery character. When they think they know who their opponent's mystery character is, players make a guess. If the guess is wrong, that player loses the game!",
        "alternatives": [
            "../../images/board-games/guesswho-img/guess-who.jpg",
            "../../images/board-games/guesswho-img/guess-2.jpg",
            "../../images/board-games/guesswho-img/guess-3.jpg"
        ]
    },
    {
        "id": 21,
        "image": "../../images/board-games/candyland-img/candyland.jpg",
        "title": "Candy Land: Kingdom of sweet adventures",
        "rating": 5,
        "reviews": 14,
        "price": 12.00,
        "stock": 4,
        "inCart": 0,
        "description": "This adorable version of the classic Candy Land game features gingerbread men as movers, colored cards, and fun illustrations that kids love, with different destinations like Cookie Commons and the chunky Chocolate Mountain.",
        "alternatives": [
            "../../images/board-games/candyland-img/candyland.jpg",
            "../../images/board-games/candyland-img/candy-land-2.jpg",
            "../../images/board-games/candyland-img/candy-land-3.jpg"
        ]
    },
    {
        "id": 22,
        "image": "../../images/board-games/twister-img/twister.jpg",
        "title": "Twister",
        "rating": 4,
        "reviews": 50,
        "price": 16.00,
        "stock": 3,
        "inCart": 0,
        "description": "Twister game challenges you to put your hands and feet at different places on the mat without falling over! Be the last player standing to win. Two new moves! With Spinner’s Choice, the spinner makes up a move for the other player to do. ",
        "alternatives": [
            "../../images/board-games/twister-img/twister.jpg",
            "../../images/board-games/twister-img/twister-2.jpg",
            "../../images/board-games/twister-img/twister-3.jpg",
            "../../images/board-games/twister-img/twister-4.jpg"
        ]
    },
    {
        "id": 23,
        "image": "../../images/board-games/scrabble-img/scrabble.jpg",
        "title": "Scrabble Game",
        "rating": 5,
        "reviews": 13,
        "price": 19.89,
        "stock": 5,
        "inCart": 0,
        "description": "Scrabble is the ultimate crossword game in which every letter counts. Grab your friends and take turns forming words on the board. After playing your turn, count the value of all the letters in every new word that you formed.",
        "alternatives": [
            "../../images/board-games/scrabble-img/scrabble.jpg",
            "../../images/board-games/scrabble-img/scrabble-2.jpg",
            "../../images/board-games/scrabble-img/scrabble-3.jpg",
            "../../images/board-games/scrabble-img/scrabble-4.jpg"
        ]
    },
    {
        "id": 24,
        "image": "../../images/board-games/operation-img/operation.jpg",
        "title": "Operation: Operation Game",
        "rating": 4,
        "reviews": 22,
        "price": 10.00,
        "stock": 10,
        "inCart": 0,
        "description": "Operation is a dexterity game in which you must extract silly body parts from a hapless patient. In the course of the game you acquire cards which dictate that you must remove a certain piece from the body of the patient.",
        "alternatives": ["../../images/board-games/operation-img/operation.jpg", "../../images/board-games/operation-img/operation-2.jpg"]
    },
    {
        "id": 25,
        "image": "../../images/board-games/clue-img/clue.jpg",
        "title": "Clue",
        "rating": 5,
        "reviews": 23,
        "price": 10.00,
        "stock": 15,
        "inCart": 0,
        "description": "The classic detective game! In Clue, players move from room to room in a mansion to solve the mystery of: who done it, with what, and where? Players are dealt character, weapon, and location cards after the top card from each card type is secretly placed in the confidential file in the middle of the board.",
        "alternatives": [
            "../../images/board-games/clue-img/clue.jpg",
            "../../images/board-games/clue-img/clue-2.jpg",
            "../../images/board-games/clue-img/clue-3.jpg",
            "../../images/board-games/clue-img/clue-4.jpg"
        ]
    },
    {
        "id": 26,
        "image": "../../images/board-games/life-img/life.jpg",
        "title": "Game of LIFE",
        "rating": 4,
        "reviews": 12,
        "price": 22.00,
        "stock": 1,
        "inCart": 0,
        "description": "Hit the road for a roller-coaster life of adventure, family, unexpected surprises, and pets! Want to take the family path, start a career, or venture down a risky road? In this game, players can make their own exciting choices as they move through the twists and turns of life. ",
        "alternatives": ["../../images/board-games/life-img/life.jpg", "../../images/board-games/life-img/life-2.jpg"]
    },
    {
        "id": 27,
        "image": "../../images/board-games/connect4-img/connect4.jpg",
        "title": "Connect 4: Four in a row",
        "rating": 3,
        "reviews": 30,
        "price": 12.50,
        "stock": 1,
        "inCart": 0,
        "description": "Challenge a friend to disc-dropping fun with the classic game of Connect 4! Drop your red or yellow discs in the grid and be the first to get 4 in a row to win. If your opponent is getting too close to 4 in a row, block them with your own disc! Whoever wins can pull out the slider bar to release all the discs and start the fun all over again!",
        "alternatives": [
            "../../images/board-games/connect4-img/connect4.jpg",
            "../../images/board-games/connect4-img/connect4-2.jpeg",
            "../../images/board-games/connect4-img/connect4-3.jpg"
        ]
    }
]