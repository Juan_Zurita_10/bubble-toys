let stockbabytoys = [
  {
    "id": 10,
    "image": "../../images/baby-toys/plush-puppy-img/plushpuppy.jpg",
    "title": "Laugh & Learn Plush Puppy",
    "rating": 5,
    "reviews": 50,
    "price": 20.00,
    "stock": 5,
    "inCart": 0,
    "description": "Say hello to your baby's first best friend! Puppy is soft, cuddly, and he's ready to make sure learning fun never ends. He responds to your baby's touch with exciting sing-along songs and phrases that introduce more than 100 first words, parts of the body, colors, shapes and more!",
    "alternatives": [
      "../../images/baby-toys/plush-puppy-img/plushpuppy.jpg",
      "../images/baby-toys/plush-puppy-img/plushpuppy-2.jpg",
      "../images/baby-toys/plush-puppy-img/plushpuppy-3.jpg"
    ]
  },
  {
    "id": 11,
    "image": "../../images/baby-toys/rock-a-stack-img/rock-a-stack.jpg",
    "title": "Giant Rock-A-Stack Baby Toy",
    "rating": 4,
    "reviews": 20,
    "price": 22.00,
    "stock": 10,
    "inCart": 0,
    "description": "Your baby can grasp and shake the six colorful rings, then stack 'em up high on the wibbly-wobbly base. And the littlest ring has swirling beads inside for your tot to shake and explore!",
    "alternatives": [
      "../../images/baby-toys/rock-a-stack-img/rock-a-stack.jpg",
      "../images/baby-toys/rock-a-stack-img/rock-a-stack-2.jpg",
      "../images/baby-toys/rock-a-stack-img/rock-a-stack-3.jpg",
      "../images/baby-toys/rock-a-stack-img/rock-a-stack-4.jpg"
    ]
  },
  {
    "id": 12,
    "image": "../../images/baby-toys/see-n-say-img/see-n-say.jpg",
    "title": "See 'n Say the Farmer Says",
    "rating": 3,
    "reviews": 0,
    "price": 16.99,
    "stock": 0,
    "inCart": 0,
    "description": "This classic learning toy is better than ever! Shaped like a barn, it teaches kids fun facts about 16 different animals. To start the learning fun, just select a page and then turn the arrow to select an animal and pull the lever to hear the Farmer speak.",
    "alternatives": [
      "../../images/baby-toys/see-n-say-img/see-n-say.jpg",
      "../images/baby-toys/see-n-say-img/see-n-say-2.jpg",
      "../images/baby-toys/see-n-say-img/see-n-say-3.jpg"
    ]
  },
  {
    "id": 13,
    "image": "../../images/baby-toys/bar-light-img/bar-light.jpg",
    "title": "Glow & Discover Bar Light",
    "rating": 4,
    "reviews": 30,
    "price": 22.99,
    "stock": 12,
    "inCart": 0,
    "description": "Colors, animals, lights and melodies are right at baby's fingertips during tummy time or sitting play! Make tummy time fun and engaging with the enriching content of Baby Einstein's Glow & Discover Light Bar",
    "alternatives": [
      "../../images/baby-toys/bar-light-img/bar-light.jpg",
      "../images/baby-toys/bar-light-img/bar-light-2.jpg",
      "../images/baby-toys/bar-light-img/bar-light-3.jpg"
    ]
  },
  {
    "id": 14,
    "image": "../../images/baby-toys/take-along-tunes-img/take-along-tunes.jpg",
    "title": "Take Along Tunes Musical Toy",
    "rating": 4,
    "reviews": 28,
    "price": 10.49,
    "stock": 2,
    "inCart": 0,
    "description": "Take Along Tunes Musical toy is made for little maestros. This easy to grasp, on the go toy is your little musician’s opening act and will quickly become a favorite.",
    "alternatives": [
      "../../images/baby-toys/take-along-tunes-img/take-along-tunes.jpg",
      "../images/baby-toys/take-along-tunes-img/take-along-tunes-2.jpg"
    ]
  },
  {
    "id": 15,
    "image": "../../images/baby-toys/4in1-robot-img/4in1-robot.jpg",
    "title": "4-in-1 Robot Toy",
    "rating": 5,
    "reviews": 60,
    "price": 53.99,
    "stock": 4,
    "inCart": 0,
    "description": "The 4-in-1 Ultimate Learning Bot is a transforming playtime buddy for baby featuring 4 ways to play and lots of fun music, lights and hands-on activities. ",
    "alternatives": [
      "../../images/baby-toys/4in1-robot-img/4in1-robot.jpg",
      "../images/baby-toys/4in1-robot-img/4in1-robot-2.jpg",
      "../images/baby-toys/4in1-robot-img/4in1-robot-3.jpg"
    ]
  },
  {
    "id": 16,
    "image": "../../images/baby-toys/baby-activity-center-img/baby-activity-center.jpg",
    "title": "Baby Activity Center: Interactive Play Center",
    "rating": 5,
    "reviews": 66,
    "price": 142.00,
    "stock": 3,
    "inCart": 0,
    "description": "Designed in collaboration with a pediatrician, the baby activity center supports a “whole body” approach to play and learning. The unique Discovery Window lets baby see their feet while they play to learn cause and effect",
    "alternatives": [
      "../../images/baby-toys/baby-activity-center-img/baby-activity-center.jpg",
      "../images/baby-toys/baby-activity-center-img/baby-activity-center-2.jpg",
      "../images/baby-toys/baby-activity-center-img/baby-activity-center-3.jpg"
    ]
  },
  {
    "id": 17,
    "image": "../../images/baby-toys/3-stage-img/3stage.jpg",
    "title": "3-Stage Developmental Learning Crawl Toy",
    "rating": 5,
    "reviews": 18,
    "price": 26.99,
    "stock": 0,
    "inCart": 0,
    "description": "If you're looking for toys to help baby crawl, you won't “bee” disappointed with this one! Designed to grow with your baby through three stages, our motorized crawl toy challenges and encourages little ones learning to crawl with colorful lights, energetic tunes and sweet buzzing sounds",
    "alternatives": ["../../images/baby-toys/3-stage-img/3stage.jpg", "../../images/baby-toys/3stage-img/3stage-2.jpg"]
  },
  {
    "id": 18,
    "image": "../../images/baby-toys/toddler-scooter-img/toddler-scooter.jpg",
    "title": "3-in-1 Baby Activity Push Walker to Toddler Scooter",
    "rating": 5,
    "reviews": 41,
    "price": 47.99,
    "stock": 10,
    "inCart": 0,
    "description": "Designed to grow with your little one for three stages of fun, Skip Hop's cute and colorful Ride-On Toy offers an entertaining way to encourage motor skills development, balance and coordination at playtime",
    "alternatives": [
      "../../images/baby-toys/toddler-scooter-img/toddler-scooter.jpg",
      "../images/baby-toys/toddler-scooter-img/toddler-scooter-2.jpg",
      "../images/baby-toys/toddler-scooter-img/toddler-scooter-3.jpg"
    ]
  }
]