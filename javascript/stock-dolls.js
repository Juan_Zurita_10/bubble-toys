let stockDolls = [
    {
        "id": 45,
        "image": "../../images/dolls/frozen-anna.png",
        "title": "Frozen 2 Anna Doll",
        "rating": 4,
        "reviews": 2,
        "price": 19.99,
        "stock": 20,
        "inCart": 0,
        "description": "Go on a dramatic journey with Disney Frozen 2 Anna Adventure Doll! A free spirit Anna is fiercely loyal determined and courageous",
        "alternatives": ["../../images/dolls/frozen-anna.png", "../../images/dolls/frozen-anna-2.png"]
    },

    {
        "id": 46,
        "image": "../../images/dolls/princess-moana.png",
        "title": "Disney Princess Moana Doll",
        "rating": 5,
        "reviews": 10,
        "price": 19.99,
        "stock": 40,
        "inCart": 0,
        "description": "Dressed in her iconic outfit Moana is ready for any seafaring excursion. Her fashion is not only beautiful but also designed for easy dressing in mind. Moana's headband is easy to put on and take off as well. Take Moana wherever you go and together discover new lands beyond the horizon!",
        "alternatives": ["../../images/dolls/princess-moana.png", "../../images/dolls/princess-moana-2.png"]
    },

    {
        "id": 47,
        "image": "../../images/dolls/baby-sweet.png",
        "title": "Baby Sweet Nursery",
        "rating": 0,
        "reviews": 0,
        "price": 23.99,
        "stock": 1,
        "inCart": 0,
        "description": "This beautiful soft body doll with bright green eyes is ready to be hugged and loved. She wears a one-piece pink footed sleeper with satin ruffle trim that features a dainty floral design. A matching pink hat completes her outfit.",
        "alternatives": ["../../images/dolls/baby-sweet.png", "../images/dolls/baby-sweet-2.png", "../images/dolls/baby-sweet-3.png"]
    },

    {
        "id": 48,
        "image": "../../images/dolls/baby-nursery.png",
        "title": "Baby Nursery Doll Pink",
        "rating": 0,
        "reviews": 0,
        "price": 23.99,
        "stock": 10,
        "inCart": 0,
        "description": "This beautiful soft body doll with bright blue eyes is ready to be hugged and loved. She wears a one-piece white footed sleeper with pretty floral detail. A matching hat completes her outfit.",
        "alternatives": ["../../images/dolls/baby-nursery.png", "../../images/dolls/baby-nursery-2.png", "../../images/dolls/baby-nursery-3.png"]
    },

    {
        "id": 49,
        "image": "../../images/dolls/umbrella-stroller.png",
        "title": "Macy's Umbrella Stroller",
        "rating": 0,
        "reviews": 0,
        "price": 8.99,
        "stock": 100,
        "inCart": 0,
        "description": "Take baby dolls on-the-go in style with You and Me Umbrella Stroller. The fashionable pink and teal stroller comfortably holds one doll wherever the day takes your little one and easily folds up umbrella-style when they've reached their destination.",
        "alternatives": []
    },

    {
        "id": 50,
        "image": "../../images/dolls/layette-set.png",
        "title": "Macy's Soft Layette Set",
        "rating": 3,
        "reviews": 10,
        "price": 9.99,
        "stock": 32,
        "inCart": 0,
        "description": "Have everything ready for new baby dolls with the You and Me Soft Layette Set. It comes with everything your little one needs to bring their baby home in style with matching adorable embroidered details. Sweet pink solids and floral pattern fabric.",
        "alternatives": []
    },

    {
        "id": 51,
        "image": "../../images/dolls/baby-alive.png",
        "title": "Baby Alive Glam Spa Doll",
        "rating": 5,
        "reviews": 23,
        "price": 22.99,
        "stock": 39,
        "inCart": 0,
        "description": "This beautiful soft body doll with bright blue eyes is ready to be hugged and loved. She wears a one-piece white footed sleeper with pretty floral detail. A matching hat completes her outfit.",
        "alternatives": []
    },

    {
        "id": 52,
        "image": "../../images/dolls/princess-blue.png",
        "title": "Disney Princess Young Raya",
        "rating": 0,
        "reviews": 0,
        "price": 9.93,
        "stock": 42,
        "inCart": 0,
        "description": "Inspired by Disney's Raya and the Last Dragon animated movie Young Raya and Kumandra Flower pays homage to her hopeful adolescent character. ",
        "alternatives": ["../../images/dolls/princess-blue.png", "../images/dolls/princess-blue-2.png", "../images/dolls/princess-blue-3.png"]
    },

    {
        "id": 53,
        "image": "../../images/dolls/high-chair.png",
        "title": "Macy's High Chair",
        "rating": 0,
        "reviews": 0,
        "price": 12.92,
        "stock": 17,
        "inCart": 0,
        "description": "Time to feed hungry baby dolls in the You and Me High Chair. This sweet high chair featuring adorable heart-shaped cut out details snaps together for easy assembly for little ones to practice pretend feeding fun.",
        "alternatives": []
    }
]