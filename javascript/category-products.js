const productContainer = document.getElementById('cards')
const cartCounter = document.getElementById('cartCounter')
let cartProducts = []

function addProductsToCategory(stock) {
    stock.forEach((product) => {
        const div = document.createElement('div')
            div.classList.add('card')
            div.style = 'display:flex; align-items: center; flex-direction:column'
            div.innerHTML = `
            <div class="image">
            <a href="/html/product-page.html"> <img src=${product.image} alt= "" class="cardImage"> </a>
            </div>
            <p class="title">${product.title}</p>
            
            <label class="rating-label">
            <input
              class="rating"
              max="5"
              readonly
              step="0.01"
              style="--value:${product.rating}"
              type="range"
              value= ${product.rating}>
            </label>
    
            <p class="ProductPrice">$ ${product.price}</p>
            <button id="add${product.id}" class="add-button" onclick="addProduct(${product.id})">add to bag <i class="fas fa-shopping-cart"></i></button>
            `
        productContainer.appendChild(div)
    })
}
