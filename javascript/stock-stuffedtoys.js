let stockStuffedtoys = [
    {
        "id": 54,
        "image": "/images/stuffed-toys/cowmuuv3.png",
        "title": "Stuffed Muu cow",
        "rating": 4,
        "reviews": 2,
        "price": 18,
        "stock": 20,
        "inCart": 0,
        "description": "The Muu cow will be the one who accompanies you with its enviable softness and its beautiful bovine spots",
        "alternatives": []
    },

    {
        "id": 55,
        "image": "/images/stuffed-toys/breadMan.jpeg",
        "title": "Stuffed Bread man",
        "rating": 5,
        "reviews": 10,
        "price": 8.99,
        "stock": 40,
        "inCart": 0,
        "description": "No matter children or adults will all love this plush toy for play & as a large pillow for reading, watching television, studying, and sleepy nap time. Suitable for bedroom, living room, family, office, etc.",
        "alternatives": []
    },

    {
        "id": 56,
        "image": "../../images/stuffed-toys/Doge.jpeg",
        "title": "Little Doge",
        "rating": 0,
        "reviews": 0,
        "price": 23.99,
        "stock": 1,
        "inCart": 0,
        "description": "If you're feeling down, don't worry, Shiba is here to save the day with his best hugs. Made from super soft materials and high density stuffing, Shiba is well prepared for many many hugs.",
        "alternatives": []
    },

    {
        "id": 57,
        "image": "../../images/stuffed-toys/kitty.jpeg",
        "title": "Sleepy kitty",
        "rating": 4,
        "reviews": 82,
        "price": 10.99,
        "stock": 11,
        "inCart": 0,
        "description": "The sleepy kitty a stuffed animal so cute and beautiful that it makes you want to squish it but... and you can do it!",
        "alternatives": []
    },

    {
        "id": 58,
        "image": "../../images/stuffed-toys/littleBird.jpeg",
        "title": "Stuffed Little Bird",
        "rating": 4,
        "reviews": 4,
        "price": 4,
        "stock": 100,
        "inCart": 0,
        "description": "This perky pip has a cocoa tail, vanilla tummy and rust-red bib. With a suedey pebble beak and shiny eyes, this robin's a charismatic scamp. Perch this robin on your desk for plenty of pep!",
        "alternatives": []
    },

    {
        "id": 59,
        "image": "../../images/stuffed-toys/salamandra.jpeg",
        "title": "Stuffed Axolotl",
        "rating": 5,
        "reviews": 10,
        "price": 11.99,
        "stock": 2,
        "inCart": 0,
        "description": "\nThe small axolotl with its imposing and tender presence will be a good piece for any aquatic collection of stuffed animals.",
        "alternatives": []
    },

    {
        "id": 60,
        "image": "/images/stuffed-toys/sheep.jpeg",
        "title": "Stuffed Baby sheep",
        "rating": 5,
        "reviews": 13,
        "price": 12.99,
        "stock": 19,
        "inCart": 0,
        "description": "The cute little sheep will help your children hug her and play with her for better dreams",
        "alternatives": []
    },

    {
        "id": 61,
        "image": "/images/stuffed-toys/TRex.jpeg",
        "title": "Little TRex",
        "rating": 3,
        "reviews": 3,
        "price": 7,
        "stock": 21,
        "inCart": 0,
        "description": "The wild but cuddly trex plush will be there for your children",
        "alternatives": [""]
    },

    {
        "id": 62,
        "image": "/images/stuffed-toys/uglyFish.jpeg",
        "title": "Handsome Ugly Fish",
        "rating": 5,
        "reviews": 2,
        "price": 11.99,
        "stock": 12,
        "inCart": 0,
        "description": "He was named the World's Ugliest Animal and he's facing extinction, but now you can make him your very own! Super-soft synthetic plush with polyester",
        "alternatives": []
    }
]